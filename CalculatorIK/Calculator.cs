﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorIK
{
    public class StreamCalculator
    {
        public int Sum(string equation)
        {
            if (equation == "")
                return 0;
            List<string> delimiters = new List<string> { ",", "\n" };
            if (equation.Length >= 4 && equation[0] == equation[1] && equation[1] == '/')
            {
                if (equation[2] == '[')
                {
                    string p = string.Concat(equation.TakeWhile((c) => c != ']'));
                    delimiters.Add(p.Substring(1,p.Length-2));
                }
                else
                {
                    delimiters.Add(equation[2].ToString());
                }
                equation = String.Concat(equation.SkipWhile(x => x != '\n').Skip(1));
            }

            var integers = equation.Split(delimiters.ToArray(), StringSplitOptions.None);
            int sum = 0;
            foreach (var number in integers)
            {
                if (!int.TryParse(number, out int val))
                    throw new FormatException("couldnt parse");
                if (val < 0)
                    throw new ArgumentException();
                sum += val;
            }
            return sum;
        }
    }
}
