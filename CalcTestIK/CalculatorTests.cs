using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorIK;

namespace IOtestsCalculator
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void Sum_Should_Return0_When_PassedEmptyString()
        {
            StreamCalculator sc = new StreamCalculator();
            Assert.AreEqual(0, sc.Sum(""));
        }

        [TestMethod]
        public void Sum_Should_ReturnValue_When_PassedValueString()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] values = { "2", "3", "2", "8", "0" };
            foreach (var val in values)
            {
                if (!int.TryParse(val, out int valint))
                    throw new FormatException();
                Assert.AreEqual(valint, sc.Sum(val));
            }
        }

        [TestMethod]
        public void Sum_Should_ReturnSumOf2Components_When_PassedComponentsSeparetedWithComma()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] testData = { "3,4", "3,4", "2,2", "8,7", "4,0" };
            foreach (var tuple in testData)
            {
                var values = tuple.Split(',');
                int testSum = 0;
                foreach (var val in values)
                {
                    if (!int.TryParse(val, out int valint))
                        throw new FormatException();
                    testSum += valint;
                }
                Assert.AreEqual(testSum, sc.Sum(tuple));
            }
        }


        [TestMethod]
        public void Sum_Should_ReturnSumOf2Components_When_PassedComponentsSeparetedWithNewLine()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] testData = { "3\n4", "3\n4", "2\n2", "8\n7", "4\n0" };
            foreach (var tuple in testData)
            {
                var values = tuple.Split('\n');
                int testSum = 0;
                foreach (var val in values)
                {
                    if (!int.TryParse(val, out int valint))
                        throw new FormatException();
                    testSum += valint;
                }
                Assert.AreEqual(testSum, sc.Sum(tuple));
            }
        }


        [TestMethod]
        public void Sum_Should_ReturnSumOfMultipleComponents_When_PassedComponentsSeparetedWithCombinationOfNewLinesAndCommas()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] testData = { "4,5,7\n3\n4", "3\n4", "2,2", "8\n5,7", "4\n0" };
            int[] result = { 23, 7, 4, 20, 4 };
            for (int i = 0; i < testData.Length; i++)
            {
                Assert.AreEqual(result[i], sc.Sum(testData[i]));
            }
        }

        [TestMethod]
        public void Sum_Should_ThrowException_When_PassedNegativeComponent()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] testData = { "4,-5,7\n3\n4", "3\n4", "-2,2", "-8\n5,7", "4\n0" };
            bool[] exception = { true, false, true, true, false };
            for (int i = 0; i < testData.Length; i++)
            {
                if (exception[i])
                {
                    Assert.ThrowsException<ArgumentException>(() => sc.Sum(testData[i]));
                }
                else
                {
                    try
                    {
                        sc.Sum(testData[i]);
                    }
                    catch (Exception)
                    {
                        Assert.Fail();
                    }
                }
            }
        }


        [TestMethod]
        public void Sum_Should_ReturnSumOfComponents_When_PassedCustomDelimiterInTheFirstLineAndComponentsSeparatedWithAnyDelimiter()
        {
            StreamCalculator sc = new StreamCalculator();
            string[] testData = { "//;\n4,5;7\n3\n4", "//w\n3w4", "//k\n4k0" };
            int[] result = { 23, 7, 4 };
            for (int i = 0; i < testData.Length; i++)
            {
                Assert.AreEqual(result[i], sc.Sum(testData[i]));
            }
        }

    }
}

